﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestController : SimpleController
{
    public Button buttonSocketIO;

    // Use this for initialization
    private void Start()
    {
        base.Start();//Các class Extern SimpleController thì phải gọi hàm này để lắng nghe các sự kiện network

        buttonSocketIO.onClick.AddListener(OnSocketIOClick);

        //init simple network
        SimpleClient.Init();
    }

    void OnSocketIOClick()
    {
        Debug.Log("Test Socket IO");

        //Connect Socket IO
        SimpleClient.ConnectSocketIO("http://127.0.0.1:12345/socket.io/", (int action) =>
        {
            if (action == SocketIOClient.ConnectAction.ACTION_CONNECTED)
            {
                //hide loading

                //Send bản tin chat
                CSChat cSChat = new CSChat();
                cSChat.text = "Hello Server";
                SendMessage(cSChat);
            }
            else if (action == SocketIOClient.ConnectAction.ACTION_DISCONNECTED)
            {
                //show loading
            }
            else if (action == SocketIOClient.ConnectAction.ACTION_RECONNECTED)
            {
                //hide loading
            }
            else if (action == SocketIOClient.ConnectAction.ACTION_RESCONNECTING)
            {
                //show loading
            }
        });
    }

    /**
     SOCKET IO
     */
    [Message("SC_CHAT")]
    public void OnSCChat(SCChat sCChat)
    {
        if (sCChat.IsSuccess())
        {
            Debug.Log("Tesssssssssssssst2: " + sCChat.text);
        }
        else
        {
            //todo: show dialog error
        }
    }

    /**
     DESTROY APP
     */
    public override void OnDestroy()
    {
        Debug.Log("Destroy Network");
        SimpleClient.OnDestroy();
    }
}
