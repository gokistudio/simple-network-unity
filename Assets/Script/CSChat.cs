﻿using UnityEngine;
using UnityEditor;

[Message("CS_CHAT")]
public class CSChat : MessageOut
{
    public string text { get; set; }
}