﻿using UnityEngine;
using UnityEditor;

[Message("CS_LOGIN")]
public class CSLogin : MessageOut
{
    public string token { get; set; }
}