﻿using UnityEngine;
using System.Collections.Generic;
using System.Reflection;
using System;

public abstract class SimpleController : MonoBehaviour
{
    private static string TAG = typeof(SimpleController).Name;
    private Dictionary<string, MethodInfo> eventDicInThis = new Dictionary<string, MethodInfo>();

    public virtual void Start() {
        LoadEvents();
    }
    public virtual void OnDestroy()
    {
        ClearEvents();
    }
    public void LoadEvents()
    {
        IEnumerable<MethodInfo> methods = Utils.GetMethodsWithAttribute(GetType(), typeof(MessageAttribute));
        foreach (var method in methods)
        {
            object[] attrs = method.GetCustomAttributes(typeof(MessageAttribute), false);
            foreach (object attr in attrs)
            {
                var message = (MessageAttribute)attr;
                EventManager.AddEvent(message.name, this, method);
                eventDicInThis[message.name] = method;
            }
        }
    }

    public void ClearEvents()
    {
        foreach (KeyValuePair<string, MethodInfo> pair in eventDicInThis)
        {
            EventManager.RemoveEvent(pair.Key, this);
        }
    }

    public void SendMessage(MessageOut messageOut)
    {
        SocketIOClient.SendMessage(messageOut);
    }
}