﻿using System.Collections.Generic;
using UnityEngine;
using BestHTTP.SocketIO;
using System;
using BestHTTP.SocketIO.Transports;
using Newtonsoft.Json;

public class SocketIOClient
{
    private static string TAG = typeof(SocketIOClient).Name;
    private static SocketManager socketManager;
    private static Socket socket;
    private static Action<int> connectAction;

    private static Dictionary<String, Type> messageInMap = new Dictionary<string, Type>();
    private static Dictionary<Type, String> typeByMessageInMap = new Dictionary<Type, string>();
    private static Dictionary<String, Type> messageOutMap = new Dictionary<string, Type>();

    public class ConnectAction
    {
        public const int ACTION_CONNECTED = 0;
        public const int ACTION_DISCONNECTED = 1;
        public const int ACTION_RECONNECTED = 2;
        public const int ACTION_RESCONNECTING = 3;
    }

    public static void Init()
    {
        LoadAllMessage();
    }

    public static void Connect(string serverUri, Action<int> connectAction = null)
    {
        SocketIOClient.connectAction = connectAction;
        if (socketManager == null || !socketManager.GetSocket().IsOpen)
        {
            Utils.Log(TAG, "ConnectSocketIO: " + serverUri);
            //init socket
            SocketOptions options = new SocketOptions();
            options.AutoConnect = true;
            options.ConnectWith = TransportTypes.WebSocket;
            options.Timeout = TimeSpan.FromSeconds(20);
            //connect to server
            socketManager = new SocketManager(new Uri(serverUri), options);
            socketManager.Encoder = new JsonDotNetEncoder();
            socket = socketManager.GetSocket();
            socket.Once("connect", OnConnect);
            socket.On("disconnect", OnDisconnect);
            socket.On("reconnecting", OnReconnecting);
            socket.On("reconnect", OnReconnect);
            socket.On("msg", OnMessage);

            socketManager.Open();
        }
    }

    private static void LoadAllMessage()
    {
        var assemblies = AppDomain.CurrentDomain.GetAssemblies();
        foreach (var assembly in assemblies)
        {
            if (assembly.FullName.Contains("Assembly-CSharp"))
            {
                var types = assembly.GetTypes();
                foreach (var type in types)
                {
                    if (type.IsSubclassOf(typeof(MessageBase)))
                    {
                        object[] attrs = type.GetCustomAttributes(typeof(MessageAttribute), false);
                        foreach (object attr in attrs)
                        {
                            if (attr is MessageAttribute)
                            {
                                MessageAttribute message = (MessageAttribute)attr;
                                Utils.Log(TAG, "Load message: " + message.name);
                                if (type.IsSubclassOf(typeof(MessageOut)))
                                {
                                    messageOutMap.Add(message.name, type);
                                    typeByMessageInMap.Add(type, message.name);
                                }
                                else if (type.IsSubclassOf(typeof(MessageIn)))
                                {
                                    messageInMap.Add(message.name, type);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private static void OnConnect(Socket socket, Packet packet, object[] args)
    {
        Utils.Log(TAG, "OnConnected");
        if (connectAction != null) connectAction(ConnectAction.ACTION_CONNECTED);
    }
    private static void OnReconnecting(Socket socket, Packet packet, params object[] args)
    {
        Utils.Log(TAG, "OnReconnecting");
        if (connectAction != null) connectAction(ConnectAction.ACTION_RESCONNECTING);
    }

    private static void OnReconnect(Socket socket, Packet packet, object[] args)
    {
        Utils.Log(TAG, "OnReconnect");
        if (connectAction != null) connectAction(ConnectAction.ACTION_RECONNECTED);
    }

    private static void OnDisconnect(Socket socket, Packet packet, object[] args)
    {
        Utils.Log(TAG, "OnDisconnect");
        if (connectAction != null) connectAction(ConnectAction.ACTION_DISCONNECTED);
    }

    private static void OnMessage(Socket socket, Packet packet, object[] args)
    {
        try
        {
            MessageData mesageData = JsonConvert.DeserializeObject<MessageData>(args[0].ToString());
            Type type = messageInMap[mesageData.name];
            if (type != null)
            {
                var msg = JsonConvert.DeserializeObject(mesageData.body.ToString(), type);
                MessageIn messageIn = (MessageIn)msg;
                if (messageIn.log)
                {
                    Utils.Log(TAG, "Receiver message: " + mesageData.name + " | " + JsonConvert.SerializeObject(messageIn));
                }
                messageIn.Execute();
                messageIn.PushEventThis();
            }
            else
            {
                Utils.Log(TAG, "Could not find handler for message: " + args[0].ToString());
            }
        }
        catch (Exception e)
        {
            Utils.Log(TAG, "Error when handler for message: " + args[0].ToString());
            Debug.LogException(e);
        }
    }

    public static void SendMessage(MessageOut messageOut)
    {
        try
        {
            string messageName = typeByMessageInMap[messageOut.GetType()];
            MessageData messageData = new MessageData(messageName, messageOut);
            socket.Emit("msg", JsonConvert.SerializeObject(messageData));
            Utils.Log(TAG, "Send message: " + messageName + " | " + JsonConvert.SerializeObject(messageOut));
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    public static void OnDestroy()
    {
        Utils.Log(TAG, "OnDestroy");
        if (socketManager != null)
        {
            socketManager.Close();
        }
    }
}
