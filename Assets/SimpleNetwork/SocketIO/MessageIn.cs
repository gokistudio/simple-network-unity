﻿using System;

public abstract class MessageIn : MessageBase
{
    public int status { get; set; }
    public string msg { get; set; }

    public virtual void Execute() { }

    public bool IsSuccess() { return status == 0; }

    public void SendMessage(MessageOut messageOut)
    {
        SocketIOClient.SendMessage(messageOut);
    }

    public void PushEventThis()
    {
        EventManager.PushEvent(name, new object[] { this});
    }
}

