﻿using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;

public class Utils 
{
    public static void Log(string tag, string msg)
    {
        Debug.Log(tag + ": " + msg);
    }
    public static void Error(Exception e)
    {
        Debug.LogException(e);
    }

    public static IEnumerable<MethodInfo> GetMethodsWithAttribute(Type classType, Type attributeType)
    {
        return classType.GetMethods().Where(methodInfo => methodInfo.GetCustomAttributes(attributeType, true).Length > 0);
    }
}