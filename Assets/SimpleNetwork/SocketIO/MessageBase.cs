﻿using Newtonsoft.Json;
using System;

[Serializable]
public class MessageBase
{
    [JsonIgnore]
    public string name { get; set; }
    [JsonIgnore]
    public bool log { get; set; }
    
    public MessageBase ()
	{
        object[] attrs = GetType().GetCustomAttributes(typeof(MessageAttribute), false);
        foreach (object attr in attrs)
        {
            var message = (MessageAttribute)attr;
            name = message.name;
            log = message.log;
            return;
        }
    }
}

