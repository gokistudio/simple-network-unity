﻿using System;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Assembly, AllowMultiple = true)]
public class MessageAttribute : Attribute
{
    public string name { set; get; }
    public bool log { get; set; }

    public MessageAttribute(string name, bool log = true)
    {
        this.name = name;
        this.log = log;
    }
}

