﻿using System;

public class MessageData
{
    public string name { get; set; }
    public Object body { get; set; }

    public MessageData(string messageName, MessageBase messageBody)
    {
        name = messageName;
        body = messageBody;
    }
}

