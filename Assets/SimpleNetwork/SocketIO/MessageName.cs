﻿using System;

public class MessageName
{
    public const string CS_INIT = "CS_INIT";
    public const string SC_INIT = "SC_INIT";
    public const string CS_ACTIVE_USER = "CS_ACTIVE_USER";
    public const string SC_ACTIVE_USER = "SC_ACTIVE_USER";
    public const string CS_UPDATE_PARAM = "CS_UPDATE_PARAM";
    public const string SC_UPDATE_PARAM = "SC_UPDATE_PARAM";
    public const string CS_GET_TOP_RANK = "CS_GET_TOP_RANK";
    public const string SC_GET_TOP_RANK = "SC_GET_TOP_RANK";
    public const string CS_GET_MY_RANK = "CS_GET_MY_RANK";
    public const string SC_GET_MY_RANK = "SC_GET_MY_RANK";
}

