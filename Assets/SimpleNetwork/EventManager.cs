﻿using UnityEngine;
using System.Reflection;
using System.Collections.Generic;
using System;

public class EventManager 
{
    private static string TAG = typeof(EventManager).Name;
    public static Dictionary<string, Dictionary<object, MethodInfo>> allEventDic = new Dictionary<string, Dictionary<object, MethodInfo>>();

    public static void AddEvent(string eventName, object parent, MethodInfo methodInfo)
    {
        //Utils.Log(TAG, "AddEvent: " + eventName + ", " + parent + ", " + methodInfo.Name);
        Dictionary<object, MethodInfo> eventDic = null;
        if (allEventDic.ContainsKey(eventName))
        {
            eventDic = allEventDic[eventName];
        }
        else
        {
            eventDic = new Dictionary<object, MethodInfo>();
            allEventDic[eventName] = eventDic;
        }
        eventDic.Add(parent, methodInfo);
    }

    public static void RemoveEvent(string eventName, object parent)
    {
        //Utils.Log(TAG, "RemoveEvent: " + eventName + ", " + parent);
        if (allEventDic.ContainsKey(eventName))
        {
            Dictionary<object, MethodInfo> eventDic = allEventDic[eventName];
            if (eventDic.ContainsKey(parent))
            {
                eventDic.Remove(parent);
            }
        }
    }

    public static void PushEvent(string eventName, object[] datas)
    {
        if (allEventDic.ContainsKey(eventName))
        {
            Dictionary<object, MethodInfo> eventDic = allEventDic[eventName];
            foreach(KeyValuePair<object, MethodInfo> pair in eventDic)
            {
                pair.Value.Invoke(pair.Key, datas);
            }
        }
    }
}