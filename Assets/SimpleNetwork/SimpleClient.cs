﻿using UnityEngine;
using UnityEditor;
using System;

public class SimpleClient 
{
    public static void Init()
    {
        SocketIOClient.Init();
    }

    public static void ConnectSocketIO(string serverUri, Action<int> connectAction = null)
    {
        SocketIOClient.Connect(serverUri, connectAction);
    }

    public static void SendMessage(MessageOut messageOut)
    {
        SocketIOClient.SendMessage(messageOut);
    }

    public static void OnDestroy()
    {
        SocketIOClient.OnDestroy();
    }
}